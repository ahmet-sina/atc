//
//  ATCTests.swift
//  ATCTests
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import XCTest
@testable import ATC
import APIClient

class ATCTests: XCTestCase {
	
	private let basePath = "/engincancan/case/"
	private var firstService : ServiceModel!
	private var client : APIClient!
	
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
		client = APIClient(host: "my-json-server.typicode.com")
		firstService = .init(id: 208,
							 serviceID: 208,
							 name: "Tadilat",
							 longName: "Tadilat, Dekorasyon ve İnşaat",
							 imageURL: .init(string: "https://cdn.armut.com/images/services/00208_cati_yapimi_ve_tadilati_thumb_875x500.jpg"),
							 proCount: 7851,
							 averageRating: 4.8,
							 completedJobsOnLastMonth: 14556)
    }

	
	func testHomeAllServicesFirstElement() async throws {
		let home : HomeModel = try await client.send(.get(basePath.appending("home")))
		XCTAssertEqual(home.allServices?.first?.id, firstService.id)
		XCTAssertEqual(home.allServices?.first?.serviceID, firstService.serviceID)
    }
	
	func testGetServiceDetailWithId() async throws {
		let serviceId = 208
		let service : ServiceModel = try await client.send(.get(basePath.appending("service/\(serviceId)")))
		XCTAssertEqual(service.id, firstService.id)
		XCTAssertEqual(service.serviceID, firstService.serviceID)
	}
	
	func testAPIHomeMethod() async throws {
		let homeOptional = await API.home()
		let home = try XCTUnwrap(homeOptional)
		XCTAssertEqual(home.allServices?.first?.id, firstService.id)
		XCTAssertEqual(home.allServices?.first?.serviceID, firstService.serviceID)
	}
	
	func testAPIServiceDetailWithId() async throws {
		let serviceId = 208
		let serviceOptional = await API.getService(with: serviceId)
		let service = try XCTUnwrap(serviceOptional)
		XCTAssertEqual(service.id, firstService.id)
		XCTAssertEqual(service.serviceID, firstService.serviceID)
	}
}
