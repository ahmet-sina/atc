//
//  SectionHeaderCollectionReusableView.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 5.12.2021.
//

import UIKit
import Reusable

final class SectionHeaderCollectionReusableView: UICollectionReusableView, NibReusable {
	@IBOutlet private weak var titleLabel: UILabel! {
		didSet { update() }
	}
	var title: String? {
		didSet { update() }
	}
	private func update() {
		titleLabel?.text = title
	}
}
