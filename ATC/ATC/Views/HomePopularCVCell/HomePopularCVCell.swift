//
//  HomePopularCVCell.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import UIKit
import Kingfisher
import Reusable

struct HomePopularCVCellViewModel {
	let imageURL: URL?
	let title: String?
}

final class HomePopularCVCell: UICollectionViewCell, NibReusable {

	@IBOutlet private weak var imageView: UIImageView!
	@IBOutlet private weak var titleLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
		imageView.layer.cornerRadius = 5
    }

	
	func setupUI(with viewModel: HomePopularCVCellViewModel) {
		if let imageURL = viewModel.imageURL {
			imageView.kf.setImage(with: imageURL)
		}
		titleLabel.text = viewModel.title
	}
}
