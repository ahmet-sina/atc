//
//  HomePostCVCell.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import UIKit
import Kingfisher
import Reusable

struct HomePostCVCellViewModel {
	let imageURL: URL?
	let title: String?
	let subTitle: String?
}

final class HomePostCVCell: UICollectionViewCell, NibReusable {

	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var subTitleLabel: UILabel!
	
	private let gradientLayer = CAGradientLayer()

	
	override func awakeFromNib() {
        super.awakeFromNib()
		imageView.layer.cornerRadius = 5
		imageView.layer.insertSublayer(gradient(frame: contentView.bounds), at:0)
    }

	func setupUI(with viewModel: HomePostCVCellViewModel) {
		if let imageURL = viewModel.imageURL {
			imageView.kf.setImage(with: imageURL)
		}
		titleLabel.text = viewModel.title
		subTitleLabel.text = viewModel.subTitle
	}
	
	func gradient(frame:CGRect) -> CAGradientLayer {
		let layer = CAGradientLayer()
		layer.frame = frame
		layer.startPoint = .init(x: 0, y: 0)
		layer.endPoint = .init(x: 0, y: 0.7)
		layer.colors = [
			UIColor.init(white: 0, alpha: 0), UIColor.black.cgColor]
		return layer
	}
}
