//
//  HomeAllServiceCVCell.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import UIKit
import Kingfisher
import Reusable

struct HomeAllServiceCVCellViewModel {
	let imageURL: URL?
	let title: String?
}

final class HomeAllServiceCVCell: UICollectionViewCell, NibReusable {

	@IBOutlet private weak var imageView: UIImageView!
	@IBOutlet private weak var titleLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
		backgroundColor = .systemGray6
		layer.cornerRadius = 5
    }
	
	func setup(with viewModel: HomeAllServiceCVCellViewModel) {
		if let imageURL = viewModel.imageURL {
			imageView.kf.setImage(with: imageURL)
		}
		titleLabel.text = viewModel.title
	}
}
