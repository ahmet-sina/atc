//
//  API.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 5.12.2021.
//

import Foundation
import APIClient

struct API {
	private static let basePath = "/engincancan/case/"
	private static var client = APIClient(host: "my-json-server.typicode.com")
	
	static func home() async -> HomeModel? {
		try? await client.send(.get(basePath.appending("home")))
	}

	static func getService(with id: Int) async -> ServiceModel? {
		try? await client.send(.get(basePath.appending("service/\(id)")))
	}
}
