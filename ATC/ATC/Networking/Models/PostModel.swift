//
//  PostModel.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import Foundation

// MARK: - PostModel
struct PostModel: Codable {
	let title: String?
	let category: String?
    let imageURL: URL?
	let link: URL?
	
	/// **Reviewer Comment**: We can use JSONDecoder.KeyDecodingStrategy.convertFromSnakeCase
	enum CodingKeys: String, CodingKey {
		case title
		case category
		case imageURL = "image_url"
		case link
	}
}
