//
//  ServiceModel.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import Foundation

// MARK: - ServiceModel
struct ServiceModel: Codable, Hashable {
	let id: Int?
	let serviceID: Int?
	let name: String?
	let longName: String?
	let imageURL: URL?
	let proCount: Int?
	let averageRating: Double?
	let completedJobsOnLastMonth: Int?
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(serviceID)
	}
	
	/// **Reviewer Comment**: We can use JSONDecoder.KeyDecodingStrategy.convertFromSnakeCase
	enum CodingKeys: String, CodingKey {
		case id
		case serviceID = "service_id"
		case name
		case longName = "long_name"
		case imageURL = "image_url"
		case proCount = "pro_count"
		case averageRating = "average_rating"
		case completedJobsOnLastMonth = "completed_jobs_on_last_month"
	}
}

