//
//  HomeModel.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import Foundation

// MARK: - HomeModel
struct HomeModel: Codable {
	let allServices: [ServiceModel]?
	let popular: [ServiceModel]?
	let posts: [PostModel]?
	
	enum CodingKeys: String, CodingKey {
		case allServices = "all_services"
		case popular
		case posts
	}
}
