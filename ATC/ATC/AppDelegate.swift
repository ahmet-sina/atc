//
//  AppDelegate.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		initFirst(viewController: HomeVC())
		return true
	}
	
	fileprivate func initFirst(viewController: UIViewController) {
		window = UIWindow(frame: UIScreen.main.bounds)
		window?.rootViewController = UINavigationController(rootViewController: viewController) 
		window?.makeKeyAndVisible()
	}
}

