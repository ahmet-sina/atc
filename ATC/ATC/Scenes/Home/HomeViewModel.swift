//
//  HomeViewModel.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 5.12.2021.
//

import Foundation

final class HomeViewModel {
	/// Section Count
	let sectionCount = 3
	var reloadData: (()->())?
	
	var home : HomeModel? {
		didSet {
			reloadData?()
		}
	}
	
	init() {
		Task.init { await getHome() }
	}
	
	/// Get home model from model
	private func getHome() async {
		self.home = await API.home()
	}
}
