//
//  HomeVC.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 4.12.2021.
//

import UIKit

final class HomeVC: UICollectionViewController {
	
	private let viewModel = HomeViewModel()
	
	init() {
		super.init(collectionViewLayout: compositionalLayout)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private lazy var searchController: UISearchController = {
		let sc = UISearchController(searchResultsController: nil)
		sc.searchBar.tintColor = .systemGreen
		sc.obscuresBackgroundDuringPresentation = false
		sc.searchBar.placeholder = "Which service do you need?"
		return sc
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupUI()
	}
	
	// MARK: - Setup UI
	private func setupUI(){
		view.backgroundColor = .systemBackground
		setupNavigationBarUI()
		setupCollectionView()
		viewModel.reloadData = {
			DispatchQueue.main.async {
				self.collectionView.reloadData()
			}
		}
	}
	
	private func setupCollectionView() {
		collectionView.register(cellType: HomePopularCVCell.self)
		collectionView.register(cellType: HomeAllServiceCVCell.self)
		collectionView.register(cellType: HomePostCVCell.self)
		collectionView.register(supplementaryViewType: SectionHeaderCollectionReusableView.self,
								ofKind: UICollectionView.elementKindSectionHeader)
	}
	
	let compositionalLayout = UICollectionViewCompositionalLayout { (sectionIndex, env) -> NSCollectionLayoutSection? in
		var fraction: CGFloat = 1 / 3
		
		if sectionIndex == 0 {
			fraction = 1/4
		}
		
		// Item
		var itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(fraction), heightDimension: .fractionalHeight(1))
		if sectionIndex == 1 {
			itemSize = .init(widthDimension: .fractionalWidth(1/2.5), heightDimension: .absolute(150))
		}
		if sectionIndex == 2 {
			itemSize = .init(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalHeight(1))
		}
		
		let item = NSCollectionLayoutItem(layoutSize: itemSize)
		item.contentInsets = .init(top: 8, leading: 8, bottom: 8, trailing: 8)
		
		let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
													  heightDimension: .absolute(50.0))
		let header = NSCollectionLayoutBoundarySupplementaryItem(
			layoutSize: headerSize,
			elementKind: UICollectionView.elementKindSectionHeader,
			alignment: .top)
		
		// Group
		var groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(fraction))
		
		if sectionIndex == 2 {
			groupSize = .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.5))
		}
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
		group.contentInsets = .init(top: 0, leading: 16, bottom: 0, trailing: 16)
		// Section
		let section = NSCollectionLayoutSection(group: group)
		section.boundarySupplementaryItems = [header]
		if sectionIndex != 0 {
			section.orthogonalScrollingBehavior = .continuous
		}
		
		if sectionIndex == 2 {
			section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
		}
		return section
	}
	
	
	private func setupNavigationBarUI() {
		navigationController?.navigationBar.prefersLargeTitles = true
		title = "Hizmet piş ağzıma düş"
		navigationItem.searchController = searchController
	}
	
	override func numberOfSections(in collectionView: UICollectionView) -> Int {
		viewModel.sectionCount
	}
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if section == 0 { return viewModel.home?.allServices?.count ?? 0 }
		if section == 1 { return viewModel.home?.popular?.count ?? 0 }
		if section == 2 { return viewModel.home?.posts?.count ?? 0 }
		return 0
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if indexPath.section == 0 {
			let cell: HomeAllServiceCVCell = collectionView.dequeueReusableCell(for: indexPath)
			guard let service = viewModel.home?.allServices?[indexPath.row] else { return cell }
			cell.setup(with: .init(imageURL: service.imageURL, title: service.name))
			return cell
		}
		
		if indexPath.section == 1 {
			let cell: HomePopularCVCell = collectionView.dequeueReusableCell(for: indexPath)
			guard let service = viewModel.home?.popular?[indexPath.row] else { return cell }
			cell.setupUI(with: .init(imageURL: service.imageURL, title: service.name))
			return cell
		}
		if indexPath.section == 2 {
			let cell: HomePostCVCell = collectionView.dequeueReusableCell(for: indexPath)
			guard let post = viewModel.home?.posts?[indexPath.row] else { return cell }
			cell.setupUI(with: .init(imageURL: post.imageURL, title: post.category, subTitle: post.title))
			return cell
		}
		
		return UICollectionViewCell()
	}
	
	override func collectionView(_ collectionView: UICollectionView,
								 viewForSupplementaryElementOfKind kind: String,
								 at indexPath: IndexPath) -> UICollectionReusableView {
		let header: SectionHeaderCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
																										  for: indexPath)
		var title = "All Services"
		if indexPath.section == 1 {
			title = "Populars"
		}
		if indexPath.section == 2 {
			title = "Posts"
		}
		header.title = title
		return header
	}
	
	override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if indexPath.section == 0 {
			guard let serviceID = viewModel.home?.popular?[indexPath.row].serviceID else { return }
			navigateToDetail(with: serviceID)
		}
		
		if indexPath.section == 1 {
			guard let serviceID = viewModel.home?.popular?[indexPath.row].serviceID else { return }
			navigateToDetail(with: serviceID)
		}
	}
	
	func navigateToDetail(with serviceID: Int) {
		let detailVC = DetailViewController()
		detailVC.serviceID = serviceID
		present(detailVC, animated: true)
	}
}
