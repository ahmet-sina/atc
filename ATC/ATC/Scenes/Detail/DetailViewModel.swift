//
//  DetailViewModel.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 6.12.2021.
//

import Foundation

final class DetailViewModel {
	
	private let serviceID : Int!
	
	var reloadData: (()->())?
	
	var detail : ServiceModel? {
		didSet {
			reloadData?()
		}
	}
	
	init(with serviceID: Int) {
		self.serviceID = serviceID
		Task.init { await getDetail() }
	}
	
	/// Get detail
	private func getDetail() async {
		self.detail = await API.getService(with: serviceID)
	}
}
