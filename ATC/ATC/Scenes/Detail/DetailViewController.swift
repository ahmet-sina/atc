//
//  DetailViewController.swift
//  ATC
//
//  Created by Ahmet Sina Ustem on 5.12.2021.
//

import UIKit
import Kingfisher

final class DetailViewController: UIViewController {

	@IBOutlet private weak var headerView: UIView!
	@IBOutlet private weak var headerImageView: UIImageView!
	@IBOutlet private weak var headerLabel: UILabel!
	@IBOutlet private weak var prosCountLabel: UILabel!
	@IBOutlet private weak var averageRatingLabel: UILabel!
	@IBOutlet private weak var completedJobCountLabel: UILabel!
	
	var serviceID : Int?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupUI()
		setupViewModel()
    }

	// MARK: - Setup View Model
	fileprivate func setupViewModel() {
		guard let serviceID = serviceID else { return }
		let viewModel: DetailViewModel = .init(with: serviceID)
		viewModel.reloadData = { [weak self] in
			guard let self = self else { return }
			guard let detail = viewModel.detail else { return }
			DispatchQueue.main.async {
				self.fillUI(with: detail)
			}
		}
	}
	
	// MARK: - UI
	fileprivate func setupUI() {
		headerImageView.layer.insertSublayer(gradient(frame: headerImageView.bounds), at:0)
	}
	
	fileprivate func fillUI(with detail: ServiceModel) {
		if let imageURL = detail.imageURL {
			headerImageView.kf.setImage(with: imageURL)
		}
		headerLabel.text = detail.name
		// We can hide proscount field if pro count is nil
		prosCountLabel.text = "\(detail.proCount ?? 0)"
		averageRatingLabel.text = "\(detail.averageRating ?? 0)"
		completedJobCountLabel.text = "Last month \(detail.completedJobsOnLastMonth ?? 0) job completed"
	}
	
	fileprivate func gradient(frame:CGRect) -> CAGradientLayer {
		let layer = CAGradientLayer()
		layer.frame = frame
		layer.startPoint = .init(x: 0, y: 0)
		layer.endPoint = .init(x: 0, y: 0.95)
		layer.colors = [
			UIColor.init(white: 0, alpha: 0), UIColor.black.cgColor]
		return layer
	}
	
}
